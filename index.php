<?php
include('config.php');
php?>

<html>

  <style>
    ul {
      font-size: 1.5rem;
      color: #666666;
      margin: 0 auto;
      display: table;
      padding-top: 60px;
    }

    ul span {
      color: cornflowerblue;
      margin: 0 auto;
    }

    h1 {
      font-weight: 100;
      text-align: center;
    }
  </style>

  <body>
  <h1>PHP interactive service</h1>
    <ul>
      <?php
      echo '<ol>SERVICE_DIRECTORY - <span>' . $SERVICE_DIRECTORY . '</span></ol>';
      echo '<ol>SERVICE_URL - <span>' . $SERVICE_URL . '</span></ol>';
      echo '<ol>FILE - <span>' . $CONFIG['file'] . '</span></ol>';
      echo '<ol>FILE_URL - <span>' . $CONFIG['file_url'] . '</span></ol>';
      php?>
    </ul>
  </body>
</html>
