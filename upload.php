<?php
include('config.php');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$response = array();


$file_name = $_FILES['file_upload']['name']; // upload file name
if ($file_name) {
  $relative_folder = sha1($_FILES['file_upload']['name']);
  $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
  $file_directory = $CONFIG['file'] . $relative_folder;
  $file_upload_tmp = $_FILES['file_upload']['tmp_name'];
  $file_upload = $file_directory . '/' . $file_name;

  if (!file_exists($file_directory)) {
    mkdir($file_directory, 0777, true);
  }

  // If Upload file
  if (move_uploaded_file($file_upload_tmp, $file_upload)) {

    // Interactive/ Web
    if (preg_match('(zip)', $file_extension) === 1) {

      $zip = new ZipArchive();
      if ($zip->open($file_upload) === TRUE) {
        // Extract ZIP
        $zip->extractTo($file_directory);
        $zip->close();
        // Delete ZIP
        unlink($file_upload);

        $response['type'] = 'interactive';
        $response['url'] = $CONFIG['file_url'] . $relative_folder;
      } else {
        $response['error'] = 1000;
        $response['error_msg'] = 'Error file unzip';
      }
    }

    //Image
    else if (preg_match('(jpg|jpeg|png|gif)', $file_extension) === 1) {
      $response['type'] = 'image';
      $response['url'] = $CONFIG['file_url'] . $relative_folder . '/' . $file_name;
    }

    // Audio
    else if (preg_match('(mp3|ogg|wav)', $file_extension) === 1) {
      $response['type'] = 'audio';
      $response['url'] = $CONFIG['file_url'] . $relative_folder . '/' . $file_name;
    }

    // Video
    else if (preg_match('(mp4|mov|avi)', $file_extension) === 1) {
      $response['type'] = 'video';
      $response['url'] = $CONFIG['file_url'] . $relative_folder . '/' . $file_name;
    }
    // Invalid extension
    else {
      $response['error'] = 5000;
      $response['error_msg'] = 'Invalid extension';
    }

    // Delete temporal file
    unlink($file_upload_tmp);
  }
  else {
    $response['error'] = 2000;
    $response['error_msg'] = 'Error upload';
  }
}

// Response data
echo json_encode($response, JSON_FORCE_OBJECT);
